module Main where
import Control.Monad -- import Monad in tutorial...
import System.Environment
import Text.ParserCombinators.Parsec hiding (spaces)


main :: IO ()
main = do args <- getArgs
          putStrLn (readExpr (args !! 0))
       
symbol :: Parser Char
symbol = oneOf "!$%|*+-/:<=?>@^_~#"
  
readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
  Left err -> "No match: " ++ show err
  Right val -> "Found value"

spaces :: Parser ()
spaces = skipMany1 space

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool

parseString :: Parser LispVal
parseString = do char '"'
                 x <- many (noneOf "\"")
                 char '"'
                 return $ String x -- similar to return (String x); similar to apply in Lisp
                                  

parseAtom :: Parser LispVal
parseAtom = do first <- letter <|> symbol
               rest <- many (letter <|> digit <|> symbol) -- e0 <|> e1 : e1 executed if e0 fails
               let atom = [first] ++ rest
               return $ case atom of
                 "#t" -> Bool True
                 "#f" -> Bool False
                 otherwise -> Atom atom

parseNumber :: Parser LispVal
parseNumber = many1 digit >>= return . Number . read
                            
parseExpr :: Parser LispVal
parseExpr = parseAtom
         <|> parseString
         <|> parseNumber

-- ./simple_parser (symbol) returns "Found value" instead of a bash syntax error
